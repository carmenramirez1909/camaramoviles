﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Runtime;
using Android.Widget;
using Uri = Android.Net.Uri;
using System.IO;
using Path = System.IO.Path;
using Plugin.Geolocator;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using Android.Content;
using Android.Graphics;
using Android.Provider;
using System;

namespace AndroidAzure3
{
    [Activity(Label = "Storing with Xamarin", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        int camrequestcode = 100;
        Uri FileUri;
        ImageView imagen;
        EditText txtname, txtaddress, txtage, txtmail, txtrevenue;
        FileStream streamFile;
        double lat, lon;
        Intent intent;
        protected override void OnCreate(Bundle bundle)
        {
            base.OnCreate(bundle);
           // Xamarin.Essentials.Platform.Init(this, savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            var btnStorage = FindViewById<Button>(Resource.Id.btnazure);
            var txtName = FindViewById<EditText>(Resource.Id.txtname);
            var txtAddress = FindViewById<EditText>(Resource.Id.txtaddress);
            var txtAge = FindViewById<EditText>(Resource.Id.txtage);
            var txtMail = FindViewById<EditText>(Resource.Id.txtmail);
            var txtRevenue = FindViewById<EditText>(Resource.Id.txtrevenue);
            imagen = FindViewById<ImageView>(Resource.Id.image);

            btnStorage.Click += async delegate
            {
                try
                {
                    var locator = CrossGeolocator.Current;
                    locator.DesiredAccuracy = 50;
                    var position = await locator.GetPositionAsync(TimeSpan.FromSeconds(10), null, true);
                    lat = position.Latitude;
                    lon = position.Longitude;


                }
                catch (System.Exception ex)
                {
                    Toast.MakeText(this.ApplicationContext, ex.Message, ToastLength.Long).Show();

                }
            };
            imagen.Click += delegate
            {
                try
                {
                    intent = new Intent(MediaStore.ActionImageCapture);
                    intent.PutExtra(MediaStore.ExtraOutput, FileUri);
                    StartActivityForResult(intent, camrequestcode, bundle);
                }
                catch (System.Exception ex)
                {

                    Toast.MakeText(this.ApplicationContext, ex.Message, ToastLength.Long).Show();
                }
            };
        }

        protected override void OnActivityResult(int requestCode, [GeneratedEnum] Result resultCode, Intent data)
        {
            base.OnActivityResult(requestCode, resultCode, data);
            if(requestCode == camrequestcode)
            {
                try
                {
                    if(txtname.Text != null)
                    {
                        FileUri = Android.Net.Uri.Parse(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal), txtname.Text + ".jpg"));
                        if (File.Exists(FileUri.ToString()))
                        {
                            Toast.MakeText(this.ApplicationContext, "Image name already exists", ToastLength.Long).Show();
                        }
                        else
                        {
                            streamFile = new FileStream(Path.Combine(System.Environment.GetFolderPath(System.Environment.SpecialFolder.Personal),
                                txtname.Text + ".jpg"), FileMode.Create);
                            var bitmapImage = (Android.Graphics.Bitmap)data.Extras.Get("data");
                            bitmapImage.Compress(Bitmap.CompressFormat.Jpeg, 100, streamFile);
                            streamFile.Close();
                            imagen.SetImageBitmap(bitmapImage);
                        }
                    }
                }
                catch (System.Exception ex)
                {

                    Toast.MakeText(this.ApplicationContext, ex.Message, ToastLength.Long).Show();
                }
            }
        }

    }
}